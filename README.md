# Copperhead
The game Snake written in Rust.

## Why?
To help learn Rust -lang

## Details
### Source
I followed this youtube video: 
 - https://www.youtube.com/watch?v=DnT_7M7L7vo&list=PLJbE2Yu2zumDF6BX6_RdPisRVHgzV02NW&index=8
It's an old video before the 2018 Rust standard came out.
Pretty good stuff still for getting down the bascis

### Quickstart
```bash
$ cargo run
```

### Bugs
This project has a few known bugs. Since it's for learning I'm not sure when I'll get back to fix them

 - The game keeps reseting if you don't get a proper key press in on boot
 - sometimes when you eat the food, a new one is not generated
