use piston_window::{self, PressEvent, UpdateEvent, clear, Button};

mod game;
mod draw;
mod snake;

use crate::draw::to_coord_u32;
use crate::game::Game;

const BACKGROUND_COLOR: piston_window::types::Color = [0.5, 0.5, 0.5, 0.9];

fn main() {
    let (width, height) = (20, 20);

    let mut window: piston_window::PistonWindow =
        piston_window::WindowSettings::new("Snake", [to_coord_u32(width), to_coord_u32(height)]) 
        .exit_on_esc(true)
        .build()
        .unwrap();

    let mut game: Game = Game::new(width, height);
    while let Some(event) = window.next() {
        if let Some(Button::Keyboard(key)) = event.press_args() {
            game.key_pressed(key);
        }
        window.draw_2d(&event, |c, g, _| {
            clear(BACKGROUND_COLOR, g);
            game.draw(&c, g);
        });

        event.update(|arg| {
            game.update(arg.dt);
        });
    }
}
